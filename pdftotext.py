import os
import json
from PyPDF2 import PdfFileReader
from elasticsearch import Elasticsearch
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO


def extract_information(pdf_path):
    with open(pdf_path, 'rb') as f:
        pdf = PdfFileReader(f)
        information = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()
    return information, number_of_pages


def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=True):
        interpreter.process_page(page)
    text = retstr.getvalue()
    fp.close()
    device.close()
    retstr.close()
    return text


es = Elasticsearch(['https://elastic1.smart-iam.com:9200'], http_auth=('elastic', 'elasticadminroot'),
                   verify_certs=False)
all_data = []
if __name__ == '__main__':
    working_dir = os.getcwd()
    path = os.path.join(working_dir, 'Aug 2021')
    for (root, dirs, files) in os.walk(path, topdown=True):
        for name in files:
            pdf_path = os.path.join(root, name)
            info, num_pages = extract_information(pdf_path)
            text = convert_pdf_to_txt(pdf_path)
            file_data = {"file_name": name.split(".")[0], "Author": info.author, "Subject": info.subject,
                         "Title": info.title, "Creator": info.creator,
                         "Producer": info.producer, "Number of pages": num_pages, "Path": pdf_path,
                         "Context": text.replace("\n", " ").strip()}
            # all_data.update({name.split(".")[0]: file_data})
            # all_data.append(file_data)
            es.index(index="iam-data-mprf", body=file_data)
            # print(text)

    # with open('result1.json', 'w') as fp:
    #     json.dump(all_data, fp)
    # print(all_data)
